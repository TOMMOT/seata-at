package cn.tedu.storage.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Storage {
    private Long id;
    private Long productId;     //商品id
    private Integer total;      //总数
    private Integer used;       //已售出
    private Integer residue;    //可用
    private Integer frozen;     //冻结
}
